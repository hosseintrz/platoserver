package com.example.plato;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    static Socket socket;
    static ServerSocket serverSocket;
    static ConcurrentHashMap<User, clientHandler> users;
    //static User currentUser;
    static ArrayList<GameRoom> rooms;

    public static void main(String[] args) throws IOException {
        users = new ConcurrentHashMap<>();
        serverSocket = new ServerSocket(8080);
        rooms = new ArrayList<>();
        System.out.println("server started");
        while (true) {
            socket = serverSocket.accept();
            clientHandler ch = new clientHandler(socket);
            ch.start();
            System.out.println("server connected");
        }
    }

    public static User findUser(String username) {
        for (User user : users.keySet()) {
            if (username.equals(user.getUsername()))
                return user;
        }
        return null;
    }

    public static GameRoom findRoom(String roomName) {
        GameRoom gr1 = null;
        for (GameRoom gr : rooms) {
            if (gr.getName().equals(roomName))
                gr1 = gr;
        }
        return gr1;
    }
}

class clientHandler extends Thread {
    Socket socket;
    DataInputStream dis;
    DataOutputStream dos;
    ObjectOutputStream oos;
    String fullmessage;
    String[] message;
    User currentuser;

    public clientHandler(Socket socket) {
        try {
            this.socket = socket;
            dos = new DataOutputStream(socket.getOutputStream());
            //oos = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();
        try {
            dis = new DataInputStream(socket.getInputStream());
            fullmessage = dis.readUTF();
            System.out.println("fullmessage" + fullmessage.trim());
            message = fullmessage.split(",");
            int op;
            switch (message[0]) {
                case "signUp":
                    signUp(message);
                    break;
                case "login":
                    login(message);
                    break;
                case "selectprofile":
                    setProfile(message);
                    break;
                case "addFriend":
                    addFriend(message);
                    break;
                case "friendRequestReply":
                    requestReply(message);
                    break;
                case "refresh":
                    refresh(message);
                    break;
                case "signout":
                    signout();
                    break;
                case "getRooms":
                    System.out.println("getrooms");
                    getRooms(message);
                    break;
                case "addRoom":
                    System.out.println("got addroom");
                    addRoom(message);
                    break;
                case "addPlayer":
                    addPlayer(message);
                    break;
                case "editRoom":
                    editRoom(message);
                    break;
                default:
                    System.out.println("none matched");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                System.out.println("closing socket");
                dos.close();
                dis.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void refresh(String[] message) {
        currentuser=Server.findUser(message[1]);
        if (currentuser != null) {
            try {
                oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(currentuser);
                //oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void signout() {
        currentuser = null;
    }

    private void addRoom(String[] message) {
        Game game;
        GameRoom gameroom = null;
        String gameName = message[1];
        int playersNum = Integer.parseInt(message[2]);
        String name = message[3];
        int op = Integer.parseInt(message[4]);
        currentuser=Server.findUser(message[5]);
        System.out.println(gameName);
        game = getGame(gameName);
        Object output = null;
        if (Server.rooms.stream().noneMatch(v -> v.getCreator().equals(currentuser)&& v.getGame().equals(game))) {
            gameroom = new GameRoom(game, playersNum, currentuser, name);
            Server.rooms.add(gameroom);
            if (op == 0)
                output = gameroom;
            else if (op == 1)
                output = Server.rooms;
        } else {
            output = "active room";
            System.out.println(output);
        }
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Game getGame(String gameName) {
        switch (gameName) {
            case "DotBox":
                return new DotBox();
            case "WordGuess":
                return new WordGuess();
            case "BattleShip":
                return new BattleShip();
            default:
                return new XO();
        }
    }

    private void editRoom(String[] message) {
        String prevRoomName = message[1];
        String newRoomName = message[2];
        int playerNum = Integer.parseInt(message[3]);
        String gamename = message[4];
        GameRoom room = Server.findRoom(prevRoomName);
        GameRoom newRoom = new GameRoom(getGame(gamename), playerNum, room.getCreator(), newRoomName);
        Server.rooms.set(Server.rooms.indexOf(room), newRoom);
//        Server.rooms.remove(room);
        //      Server.rooms.add(newRoom);
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(Server.rooms);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getRooms(String[] message) {
        System.out.println(message[1]);
        try {ArrayList<GameRoom> mrooms=new ArrayList<>();
            for(GameRoom gr:Server.rooms){
                if(gr.getGame().getName().equals(getGame(message[2]).getName()))
                    mrooms.add(gr);
            }
            oos = new ObjectOutputStream(socket.getOutputStream());
            //System.out.println("getting rooms=> " + Server.rooms.size());
            oos.writeObject(mrooms);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addPlayer(String[] message) {
        //int op = -1;
        /*try {
            op = dis.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        currentuser=Server.findUser(message[3]);
        boolean added=false;
        int op = Integer.parseInt(message[2]);
        System.out.println("op= "+op);
        GameRoom room = Server.findRoom(message[1]);
        Object output=null;
        if (room.getState().equals(com.example.plato.State.WAITING)) {
            System.out.println("state is waiting");
            if (room.getPlayers().contains(currentuser))
                output = "You Have already Joined This Room";
            else if(room.getCreator().equals(currentuser))
                output="you are the creator of room";
            else {added=true;
                room.addPlayer(currentuser);
                System.out.println("room current state= "+room.getState().toString());
                if (op == 0)
                    output = room;
                else if (op == 1)
                    output = Server.rooms;
            }
        } else if (room.getState().equals(com.example.plato.State.FULL))
            output = "Room Is Full";
        else
            output= Server.rooms;

        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(output);

            if(added && op==1 && room.getState().equals(com.example.plato.State.FULL)){
                room.setState(com.example.plato.State.STARTING);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void signUp(String[] message) throws IOException {
        StringBuilder replyMessage = new StringBuilder();
        String username = message[1];
        String password = message[2];
        String email = message[3];
        username = username.replaceAll("\\s", "");
        password = password.replaceAll("\\s", "");
        email = email.replaceAll("\\s", "");
        System.out.println("usrname: *" + username + "*");
        System.out.println("password: " + password);
        System.out.println("email: " + email);
        User newUser = new User(username, password, email);
        if (username.isEmpty()) {
            replyMessage.append("usernameEmpty,");
        }
        if (password.isEmpty()) {
            replyMessage.append("passwordEmpty,");
        } else if (password.length() < 6) {
            replyMessage.append("passwordShort,");
        }
        if (email.isEmpty()) {
            replyMessage.append("emailEmpty");
        }
        if (replyMessage.length() == 0) {
            if (Server.users.containsKey(newUser))
                replyMessage.append("user exists");
            else {
                replyMessage.append("welcome to plato");
                Server.users.put(newUser, this);
                currentuser = newUser;
            }
        }
        String sendMessage = replyMessage.toString();
        sendString(socket, sendMessage);
        System.out.println("current user is " + currentuser.getUsername());
    }

    private void login(String[] message) {
        System.out.println("entered login");
        StringBuilder replyMessage = new StringBuilder();
        String username = message[1];
        String password = message[2];
        username = username.replaceAll("\\s", "");
        password = password.replaceAll("\\s", "");
        User newUser = Server.findUser(username);
        try {
            if (newUser != null) {
                if (newUser.getPassword().equals(password)) {
                    //sendString(socket,"loginOk");
                    System.out.println("loginok");
                    dos.writeUTF("loginOk");
                    currentuser = newUser;
                    oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(newUser);
                } else
                    System.out.println("wrong password");
                dos.writeUTF("wrongPassword");
            } else
                System.out.println("usernot found");
            dos.writeUTF("userNotFound");
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private void addFriend(String[] message) {
        System.out.println("came in addfriend");
        StringBuilder reply = new StringBuilder();
        User user2 = Server.findUser(message[1]);
        currentuser=Server.findUser(message[2]);
        if (user2 == null)
            reply.append("userNotFound");
        else if (currentuser.equals(user2))
            reply.append("its your username");
        else if (currentuser.getFriends().contains(user2)) {
            reply.append("alreadyExists");
        } else {
            user2.addFriendRequests(currentuser);
            reply.append("request sent");
        }
        if (user2 != null)
            System.out.println(currentuser.getUsername() + " requested " + user2.getUsername());
        try {
            sendString(socket, reply.toString());
            System.out.println(reply.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestReply(String[] message) {
        User user2 = Server.findUser(message[1]);
        currentuser=Server.findUser(message[3]);
        System.out.println(currentuser.getUsername() + " replied to" + user2.getUsername() + " request");
        if (message[2].equals("true")) {
            currentuser.addFriend(user2);
            user2.addFriend(currentuser);
            System.out.println("accepted");
        }
        currentuser.removeFriendRequest(user2);
        System.out.print("friend reqs: ");
        for (User u : currentuser.getFriendRequests())
            System.out.print(u.getUsername() + " ");
        System.out.println();
        System.out.print("friends: ");
        for (User u : currentuser.getFriends())
            System.out.print(u.getUsername() + " ");
    }

    private void setProfile(String[] message) throws IOException {
        String answer;
        dis = new DataInputStream(socket.getInputStream());
        int n = dis.readInt();
        byte[] b = new byte[n];
        //dis.read(b);
        //String mes=new String(b,"UTF-8");
        //message=mes.split(",");
        dis.readFully(b);
        String mes = new String(b);
        message = mes.split(",");
        System.out.println("mes0=" + message[0]);
        System.out.println("mes1=" + message[1]);
        System.out.println(message[2].length());
        User user = Server.findUser(message[1]);
        if (user != null) {
            user.setEncodedProfile(message[2]);
            answer = "received";
            System.out.println("Set profile from " + user.getUsername());
        } else {
            answer = "Notuploded";
        }
        // dis.close();

        //send answer for user
        Thread r = new Thread(() -> {
            try {
                dos.writeUTF(answer);
                System.out.println("wrote answer to dos");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        r.start();
        try {
            r.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private synchronized void sendString(Socket socket, String message) {
        //dos = new DataOutputStream(socket.getOutputStream());
        Thread sendstring = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        sendstring.start();
        try {
            sendstring.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

   /* private synchronized void sendObject(Socket socket, Object obj) {
        try {
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread sendobject = new Thread(() ->
        {
            try {
                System.out.println("sending object");
                oos.writeObject(obj);
                System.out.println(((User)obj).getUsername());
                oos.flush();
                System.out.println("obj sent");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        sendobject.start();
        try {
            sendobject.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("getting out of thread sending obj");
        //close output stream
    }*/
}
