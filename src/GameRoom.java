package com.example.plato;

import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

enum State {
    WAITING,
    FULL,
    STARTING,
    RUNNING,
    FINISHED
}

public class GameRoom implements Serializable {
    public static final Long serialVersionUID = 3421421L;
    private String name;
    private Game game;
    private int playersNum;
    private int playerLimit;
    private State state;
    private ArrayList<User> players;
    private User creator;

    public GameRoom(Game game, int playerLimit, User creator, String name) {
        this.game = game;
        this.name = name;
        this.playersNum = 1;
        this.playerLimit = playerLimit;
        this.creator = creator;
        this.state = State.WAITING;
        players=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<User> getPlayers() {
        return players;
    }

    public User getCreator() {
        return creator;
    }

    public State getState() {
        return state;
    }

    public Game getGame() {
        return game;
    }

    public int getPlayersNum() {
        return playersNum;
    }

    public void setState(State state){
        this.state=state;
    }
    public int getPlayerLimit() {
        return playerLimit;
    }

    public void addPlayer(User user) {
        this.players.add(user);
        playersNum++;
        if(playersNum==playerLimit)
            this.state=State.FULL;
    }

    public void removePlayer(User user) {
        this.players.remove(user);
    }

    public void setPlayerLimit(int n) throws UserNumException {
        if (n >= playersNum)
            this.playerLimit = n;
        else
            throw new UserNumException("there are " + n + " players in room");
    }

    /*public void sendMessage(String message) {

    }*/

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;
        else {
            GameRoom gr = (GameRoom) obj;
            if (!gr.getName().equals(this.getName()) || !gr.getCreator().equals(this.getCreator()))
                return false;
            else
                return true;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, creator);
    }
}

class UserNumException extends Exception {
    public UserNumException(String s) {
        super(s);
    }
}